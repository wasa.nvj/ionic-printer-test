package io.ionic.starter;

import android.content.Context;

import com.getcapacitor.JSArray;
import com.getcapacitor.JSObject;
import com.getcapacitor.NativePlugin;
import com.getcapacitor.Plugin;
import com.getcapacitor.PluginCall;
import com.getcapacitor.PluginMethod;

import com.starmicronics.stario.PortInfo;
import com.starmicronics.stario.StarIOPort;
import com.starmicronics.stario.StarIOPortException;
import com.starmicronics.stario.StarPrinterStatus;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

@NativePlugin()
public class PrinterSM230 extends Plugin {
    public static StarIOPort mPort;
    public static String  mPortName = null;
    public static String  mPortSettings;
    public static int     mTimeout;
    public static Context mContext;
    public static byte[] mCommands;

    @PluginMethod()
    public void List(PluginCall call) {
        ArrayList<String> printers = List();
//        String message = call.getString("message");
        // More code here...
        JSArray array = new JSArray();
        for (String printer : printers) {
            // do something with object
            JSObject jobj = new JSObject();
            jobj.put("name", printer);
            array.put(jobj);
        }

        JSObject result = new JSObject();
        result.put("printers", array);
        call.success(result);
    }

    @PluginMethod()
    public void Print(PluginCall call) throws StarIOPortException, JSONException {
        try
        {
            JSArray jArray = call.getArray("words", new JSArray());
            List<String> words = jArray.toList();
            Print(call.getString("portName"), words, call.getString("align"));
            call.success();
        }
        catch (Throwable ex)
        {
            String result = ex.toString();
        }
    }

    @PluginMethod()
    public void Status(PluginCall call) {
        String result = "OFFLINE";
        try
        {
            String portName = call.getString("portName");
            mPortSettings = "mini";
            mTimeout = 500;
            mPort = StarIOPort.getPort(portName, mPortSettings, mTimeout, null);
            if (mPort != null) {
                StarIOPort.releasePort(mPort);
                result = "ONLINE";
                mPort = null;
            }
        }
        catch (Throwable ex)
        {
            result = ex.toString();
        }

        JSObject jsonResult = new JSObject();
        jsonResult.put("status", result);
        call.success(jsonResult);
    }

    private static byte[] convertFromListByteArrayTobyteArray(ArrayList<byte[]> ByteArray) {
        int dataLength = 0;
        for (int i = 0; i < ByteArray.size(); i++)
            dataLength += ((byte[])ByteArray.get(i)).length;
        int distPosition = 0;
        byte[] byteArray = new byte[dataLength];
        for (int j = 0; j < ByteArray.size(); j++) {
            System.arraycopy(ByteArray.get(j), 0, byteArray, distPosition, ((byte[])ByteArray.get(j)).length);
            distPosition += ((byte[])ByteArray.get(j)).length;
        }
        return byteArray;
    }

    public static void Print(String portName, List<String> words, String align) throws StarIOPortException {
        try
        {
            mPortSettings = "mini";
            mTimeout = 10000;
            mPort = StarIOPort.getPort(portName, mPortSettings, mTimeout, null);
            StarPrinterStatus status;
            status = mPort.beginCheckedBlock();
            if (status.offline) {
                throw new StarIOPortException("A printer is offline.");
            }

            ArrayList<byte[]> list = new ArrayList<byte[]>();
            switch (align)
            {
                case "center": list.add(new byte[] { 27, 97, 49 }); break;
                case "left": list.add(new byte[] { 27, 97, 48 }); break;
            }

            for(String word : words)
            {
                list.add(word.getBytes());
            }

            mCommands = convertFromListByteArrayTobyteArray(list);
            mPort.writePort(mCommands, 0, mCommands.length);
            if (mPort != null) {
                StarIOPort.releasePort(mPort);
            }
        }
        catch (Throwable ex)
        {
            String result = ex.toString();
        }
    }

    public static ArrayList<String> List()
    {
        List<PortInfo> BTPortList = null;

        String result = "";
        ArrayList<PortInfo> arrayDiscovery = new ArrayList();
        ArrayList<String> arrayPortName = new ArrayList();
        try
        {
            BTPortList = StarIOPort.searchPrinter("BT:");
            for (PortInfo portInfo : BTPortList) {
                arrayDiscovery.add(portInfo);
            }
            for (PortInfo discovery : arrayDiscovery)
            {
                String portName = discovery.getPortName();
                if (!discovery.getMacAddress().equals("")) {
//                    portName = portName + "@!?;-" + discovery.getMacAddress();
                    portName =   "BT:" + discovery.getMacAddress();
                }
                arrayPortName.add(portName);
//                result = result + portName + "@!?;,";
            }
        }
        catch (StarIOPortException e)
        {
            e.printStackTrace();
        }

        return arrayPortName;
    }
}