# To Start Project

```
npm install
```

```
npm install -g @ionic/cli
```

# Run Project in Local (Web)

```
ionic serve
```

# Run Project in Native

```
ionic capacitor run ios -l --external
```

```
ionic capacitor run android -l --host=YOUR_IP_ADDRESS
```

# Note

Detail Description

iOS Capacitor https://ionicframework.com/docs/developing/ios

Android Capacitor https://ionicframework.com/docs/developing/android

# Add Cordova Plugins into Capacitor Project

https://ionicframework.com/docs/native/community

ionic cap sync

# Fix Issue print pdf from base64: with android
path android\capacitor-cordova-android-plugins\src\main\java\de\appplant\cordova\plugin\printer\PrintContent.java

line 93   mime = URLConnection.guessContentTypeFromStream(io.openBase64(path));
change to 
        mime = "application/pdf";
