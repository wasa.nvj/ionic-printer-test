import Capacitor

@objc(TestNative)
public class TestNative: CAPPlugin {
  @objc func echo(_ call: CAPPluginCall) {
    let value = call.getString("value") ?? ""
    var searchPrinterResult: [PortInfo]? = nil
    do {
        searchPrinterResult = try SMPort.searchPrinter(target: "BT:")  as? [PortInfo]
        guard let portInfoArray: [PortInfo] = searchPrinterResult else {
            // self.tableView.reloadData()
            return
        }

        for portInfo: PortInfo in portInfoArray {

        }
    }
    catch let error as NSError {
        var code = error.code
    }
    call.success([
        "value": value
    ])
  }
}
