import Capacitor

@objc(PrinterSM230)
public class PrinterSM230: CAPPlugin {
 @objc func List(_ call: CAPPluginCall) {
    var result: Array<[String:String]> = []
    var searchPrinterResult: [PortInfo]? = nil
    do {
        searchPrinterResult = try SMPort.searchPrinter(target: "BT:PRNT Star")  as? [PortInfo]
        guard let portInfoArray: [PortInfo] = searchPrinterResult else {
            return
        }
        
        for portInfo: PortInfo in portInfoArray {
            result.append(["name": portInfo.portName])
        }
    }
    catch {

    }

    call.success(["printers": result])
 }
 @objc func Status(_ call: CAPPluginCall) {
     var result = "OFFLINE"
     let portName = call.getString("portName");
     var mPort = try? SMPort.getPort(portName: portName, portSettings: "mini", ioTimeoutMillis: 10000);
         // Sleep to avoid a problem which sometimes cannot communicate with Bluetooth.
         // (Refer Readme for details)
     if #available(iOS 11.0, *), ((portName?.uppercased().hasPrefix("BT:")) != nil) {
             Thread.sleep(forTimeInterval: 0.2)
         }
     if (mPort != nil) {

         SMPort.release(mPort);
         result = "ONLINE";
         mPort = nil;
     }
    
     call.success(["status": result]);
 }

  @objc func Print(_ call: CAPPluginCall) {
    do {
         var portName = call.getString("portName");
         var words = call.getArray("words", String.self) ?? [String]();
         var align = call.getString("align");
         var mTimeout = 10000;
         var mPort =  try SMPort.getPort(portName: portName, portSettings: "mini", ioTimeoutMillis: 10000);
         // Sleep to avoid a problem which sometimes cannot communicate with Bluetooth.
         // (Refer Readme for details)
         if #available(iOS 11.0, *), ((portName?.uppercased().hasPrefix("BT:")) != nil) {
                Thread.sleep(forTimeInterval: 0.2)
            }
         var printerStatus: StarPrinterStatus_2 = StarPrinterStatus_2()
         try mPort.beginCheckedBlock(starPrinterStatus: &printerStatus, level: 2)
         
//         if printerStatus.offline == sm_true {
//             break
//         }

        var list: [UInt8] = [];
        if (align == "center") {
            list.append(contentsOf: [27, 97, 49])
        } else if (align == "left") {
            list.append(contentsOf: [27, 97, 48])
        }
         
         for word in words {
            print("Hello, \(word)")
            list.append(contentsOf: Array(word.utf8))
         }
        var written: UInt32 = 0
        try mPort.write(writeBuffer: list, offset: 0, size: UInt32(UInt(list.count)), numberOfBytesWritten: &written );
        SMPort.release(mPort);
          call.success();
     }
     catch let error as NSError {
         var code = error.code
     }
 }
}

