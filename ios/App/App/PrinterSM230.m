#import <Foundation/Foundation.h>
#import <Capacitor/Capacitor.h>

CAP_PLUGIN(PrinterSM230, "PrinterSM230",
           CAP_PLUGIN_METHOD(List, CAPPluginReturnPromise);
           CAP_PLUGIN_METHOD(Status, CAPPluginReturnPromise);
           CAP_PLUGIN_METHOD(Print, CAPPluginReturnPromise);
)
